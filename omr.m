% Processamento de imagens
% Projeto OMR
% 
% Alunos:
%       Davi Silva      0973955
%       Guilherme Lima  1360841

clc, clear variables, close all;

imgFilename = 'Imagens/p31.jpg';

img = imread(imgFilename);

% Process Image
result = processImage(img);

% Evaluate result

load('GABARITO.mat');
answersCorrected = zeros(50, 4);
answerColor = zeros(50, 3);
grade = 0;
aux = 1;
for i = 1:50
    if sum(result.answers(i,:).*gabarito(i, 1:5, versionValue)) == 0
        answersCorrected(aux, :) = result.answersBoundingBox(i, :);
        answerColor(aux,:) = [1;0;0];
        aux = aux + 1;
        
        answersCorrected(aux, :) = gabarito(i, 6:9, versionValue);
        answerColor(aux,:) = [0;1;0];
        aux = aux + 1;
    else
        answersCorrected(aux, :) = result.answersBoundingBox(i, :);
        answerColor(aux,:) = [0;1;0];
        aux = aux + 1;

        grade = grade + 1*gabarito(i, 10, versionValue);
    end
end


% Plot
imgTransformed = findImageTransformed(img, result.refPoints);
figure
imshow(reult.imgTransformed), title(sprintf('Test results against answer sheet %d: %d%%', versionValue, grade))

hold on
rectangle('Position', result.versionBoundingBox,'EdgeColor','b','LineWidth',1)
for i = 1:size(answersCorrected, 1)
    rectangle('Position', answersCorrected(i,:),'EdgeColor', answerColor(i,:),'LineWidth',1)
end
hold off




%% Refactor

% Plot
% figure
% subplot(2,3,1), imshow(img), title('Original')
% subplot(2,3,2), imshow(imgBW), title('Binarized')
% subplot(2,3,3), imshow(imgMorph), title('Morphological operations')
% 
% hold on
% for i = 1:4
%     rectangle('Position',refPoints(i, :),'EdgeColor','r','LineWidth',1)
% end
% hold off
% 
% subplot(2,3,4), imshow(imgTransformed), title('Transformed')
% 
% hold on
% for i = 1:50
%     rectangle('Position', questionsPosition(i,:),'EdgeColor','r','LineWidth',1)
% end
% hold off
% 
% question = imcrop(imgTransformed, questionsPosition(1,:));
% subplot(2,3,5), imshow(question), title('First question');
% [answer, imgBW, imgMorph] = findAnswer(question);
% subplot(2,3,6), bar(categorical({'a','b','c','d','e'}), answer), title('Answer')




% Show all questions
% figure
% for i = 1:50
%     question = imcrop(imgTransformed, questions(i,:));
%     subplot(5,10,i), imshow(question), title(sprintf('Question %d', i));
% end

% Show how find reference points works
% [refPoints, qBW, qMorph] = findRefPoints(img);
% figure
% subplot(1,3,1), imshow(img), title('Input img')
% subplot(1,3,2), imshow(qBW), title('BW')
% subplot(1,3,3), imshow(qMorph), title('Morphological operations')
% hold on
% plot(refPoints(:,1), refPoints(:,2), 'r*');
% hold off

% Show how find answer works
% img = imgTransformed;
% question = imcrop(img, questions(2,:));
% [answer, qBW, qMorph] = findAnswer(question);
% figure
% subplot(1,4,1), imshow(question), title('Question')
% subplot(1,4,2), imshow(qBW), title('Question BW')
% subplot(1,4,3), imshow(qMorph), title('Morphological operations')
% letters = categorical({'a','b','c','d','e'});
% subplot(1,4,4), bar(letters, answer), title('Answer')
