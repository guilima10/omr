% Processamento de imagens
% Projeto OMR
% 
% Alunos:
%       Davi Silva      0973955
%       Guilherme Lima  1360841

clc, clear variables, close all;

imgFilename = 'Imagens/p31.jpg';

img = imread(imgFilename);

% Process Image
result = processImage(img);

% Save answer sheet
load('GABARITO.mat');
load('QUESTIONS_WEIGHT.mat');
gabarito(:, :, result.versionValue) = [result.answers result.answersBoundingBox questionsWeight];
save('GABARITO.mat', 'gabarito');

% Plot
imgTransformed = findImageTransformed(img, result.refPoints);
figure
imshow(imgTransformed), title(sprintf('Answers loaded into answer sheet: %d', result.versionValue))

hold on
rectangle('Position', result.versionBoundingBox,'EdgeColor','r','LineWidth',1)
for i = 1:50
    rectangle('Position', result.answersBoundingBox(i,:),'EdgeColor','r','LineWidth',1)
end
hold off
