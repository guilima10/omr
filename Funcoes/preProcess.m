function [ imgBW, imgGray ] = preProcess( img )
% Processamento de imagens
% Projeto OMR
% 
% Alunos:
%       Davi Silva      0973955
%       Guilherme Lima  1360841

imgGray = rgb2gray(img);
imgGray = imgaussfilt(imgGray, 2.5);
imgBW = imbinarize(imgGray, 'adaptive', 'ForegroundPolarity', 'bright', 'sensitivity', 0.8);
imgBW = imcomplement(imgBW);

end

