function [ morphSize ] = findMorphOpSize( imgBW, maxNumObj, opSize )
% Processamento de imagens
% Projeto OMR
% 
% Alunos:
%       Davi Silva      0973955
%       Guilherme Lima  1360841

%Find best morphological operation size
% figure
prevMorphSize = opSize(2);
sizes = opSize(1):1:opSize(2);
for i = 1:length(sizes)
    se = strel('square', sizes(i));
    imgOperated = imopen(imgBW, se);
    [~, numObj] = bwlabel(imgOperated);
    
%     text = sprintf('op:%d-num:%d', sizes(i), numObj);
%     subplot(6, 6, i), imshow(imgOperated), title(text)
    
    if numObj > maxNumObj
        prevMorphSize = sizes(i);
    elseif numObj == maxNumObj
        % Success
        morphSize = sizes(i);
        return ;
    else
        % Error: there is no morph operation that finds the desired number
        % of objects
        morphSize = prevMorphSize;
        return ;
    end
end

% Error: there is no morph operation that finds the desired number
% of objects
morphSize = prevMorphSize;

end

