function [ questions ] = findQuestionsPosition( )
% Processamento de imagens
% Projeto OMR
% 
% Alunos:
%       Davi Silva      0973955
%       Guilherme Lima  1360841

firstColumnPosition = [70 50];
secondColumnPosition = [423 50];
questionSize = [185 29.5];

questionsYposition = [zeros(25,1),((1:25)'-1)*questionSize(2)];
questions = zeros(50, 4);
questions(1:25,1:2) = repmat(firstColumnPosition,25,1) + questionsYposition;
questions(26:50,1:2) = repmat(secondColumnPosition,25,1) + questionsYposition;
questions(1:50,3:4) = repmat(questionSize, 50, 1);
end

