function [ img ] = findImageTransformed( movingImage, referencePoints )
% Processamento de imagens
% Projeto OMR
% 
% Alunos:
%       Davi Silva      0973955
%       Guilherme Lima  1360841

points(1:4,1:2) = referencePoints(1:4, 1:2);
load('BASE_POINTS_4');

tf = fitgeotrans(points, BASE_POINTS, 'projective');
transformReference = imref2d([800 600], [275 1315], [247 2055]);
img = imwarp(movingImage,tf,'OutputView',transformReference);
end

