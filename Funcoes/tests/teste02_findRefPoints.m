clc, clear variables, close all;

load('IMGS.mat');
numImgs = size(imgs, 2);
figure
for idx = 1:numImgs
    name = sprintf('aux/%sBW.jpg', imgs(idx));
    imgBW = imbinarize(imread(name));
    [refPoints, imgMorph] = findRefPoints(imgBW);
    save(sprintf('aux/REF_POINTS_%s.mat', imgs(idx)), 'refPoints');

    subplot(2,numImgs,idx), imshow(imgBW)
    hold on
    plot(refPoints(:, 1), refPoints(:, 2), 'gx')
    hold off
    
    subplot(2,numImgs,idx+1*numImgs), imshow(imgMorph)
    hold on
    for i = 1:4
        rectangle('Position',refPoints(i, :),'EdgeColor','r','LineWidth',1)
    end
    hold off
end
