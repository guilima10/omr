clc, clear variables, close all;

load('IMGS.mat');
numImgs = size(imgs, 2);
figure
for idx = 1:numImgs
    name = sprintf('aux/%sBW.jpg', imgs(idx));
    imgBW = imbinarize(imread(name));
    load(sprintf('aux/REF_POINTS_%s.mat', imgs(idx)));
    
    imgTransformed = findImageTransformed(imgBW, refPoints);
    imwrite(imgTransformed, sprintf('aux/%sTransformed.jpg', imgs(idx)));

    subplot(2,numImgs,idx), imshow(imgBW)
    subplot(2,numImgs,idx+1*numImgs), imshow(imgTransformed)
end
