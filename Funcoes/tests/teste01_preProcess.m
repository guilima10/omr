clc, clear variables, close all;

load('IMGS.mat');
numImgs = size(imgs, 2);
figure
for i = 1:numImgs
    name = sprintf('Imagens/%s.jpg', imgs(i));
    img = imread(name);
    [imgBW, imgGray] = preProcess(img);
    imwrite(imgBW, sprintf('aux/%sBW.jpg', imgs(i)));

    subplot(3,numImgs,i), imshow(img)
    subplot(3,numImgs,i+1*numImgs), imshow(imgGray)
    subplot(3,numImgs,i+2*numImgs), imshow(imgBW)
end
