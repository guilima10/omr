clc, clear variables, close all;

load('IMGS.mat');
numImgs = size(imgs, 2);
numQuestions = 5;
for idx = 1:numImgs
    name = sprintf('aux/%sTransformed.jpg', imgs(idx));
    imgTransformed = imbinarize(imread(name));
    
    % Process questions
    load('QUESTIONS_POSITION.mat');
    answers = zeros(numQuestions,5);
    answersBoundingBox = zeros(numQuestions, 4);
    figure
    for i = 1:numQuestions
        qBW = imcrop(imgTransformed, questionsPosition(i,:));
        [answers(i,:), ~, answersBoundingBox(i,:), qMorph] = findAnswer(qBW);
%         answersBoundingBox(i,1:2) = answersBoundingBox(i,1:2) + questionsPosition(i,1:2);
        
        
        subplot(2,numQuestions,i), imshow(qBW)
        rectangle('Position',answersBoundingBox(i, :),'EdgeColor','r','LineWidth',1)
        
        subplot(2,numQuestions,i+1*numQuestions), imshow(qMorph)
        rectangle('Position',answersBoundingBox(i, :),'EdgeColor','r','LineWidth',1)
    end
end
