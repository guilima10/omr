function [ result ] = processImage( img )
% Processamento de imagens
% Projeto OMR
% 
% Alunos:
%       Davi Silva      0973955
%       Guilherme Lima  1360841

imgBW = preProcess(img);
refPoints = findRefPoints(imgBW);
imgTransformed = findImageTransformed(imgBW, refPoints);

result = struct;
result.refPoints = refPoints;

% Process test version
load('VERSION_POSITION.mat');
vBW = imcrop(imgTransformed, versionPosition);
[version, versionValue, versionBoundingBox] = findAnswer(vBW);
versionBoundingBox(1:2) = versionBoundingBox(1:2) + versionPosition(1:2);

result.versionValue = versionValue;
result.versionBoundingBox = versionBoundingBox;

% Process questions
load('QUESTIONS_POSITION.mat');
answers = zeros(50,5);
answersBoundingBox = zeros(50, 4);
for i = 1:50
    qBW = imcrop(imgTransformed, questionsPosition(i,:));
    [answers(i,:), ~, answersBoundingBox(i,:)] = findAnswer(qBW);
    answersBoundingBox(i,1:2) = answersBoundingBox(i,1:2) + questionsPosition(i,1:2);
end

result.answers = answers;
result.answersBoundingBox = answersBoundingBox;

end

