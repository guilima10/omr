function [ refPoints, imgMorph ] = findRefPoints( imgBW )
% Processamento de imagens
% Projeto OMR
% 
% Alunos:
%       Davi Silva      0973955
%       Guilherme Lima  1360841

se = strel('square', findMorphOpSize(imgBW, 4, [40 65]));
imgMorph = imopen(imgBW, se);

% Image Labeling
s = regionprops(imgMorph, 'BoundingBox');
points = cat(1, s.BoundingBox);
pointsLeft = sortrows(points(1:2,:), 2);
pointsRight = sortrows(points(3:4,:), 2);
refPoints = [pointsLeft; pointsRight];
end

