function [ answer, answerValue, boundingBox, qMorph ] = findAnswer( qBW )
% Processamento de imagens
% Projeto OMR
% 
% Alunos:
%       Davi Silva      0973955
%       Guilherme Lima  1360841

qBW = imclose(qBW, strel('square', 2));
se = strel('square', findMorphOpSize(qBW, 1, [1 15]));
qMorph = imopen(qBW, se);

% X centroid
% a =   4 -> 28
% b =  42 -> 66
% c =  81 -> 105
% d = 119 -> 143
% e = 158 -> 180
s = regionprops(qMorph, 'Centroid', 'BoundingBox');
boundingBox = cat(1, s.BoundingBox);

centroids = cat(1, s.Centroid);
centroid = centroids(1,1);

numCentroids = size(centroids, 1);
if numCentroids > 1 || numCentroids == 0
    answer = [0 0 0 0 0];
    answerValue = 0;
    boundingBox = [0 0 0 0];
    return ;
end

if centroid <= 35
    answer = [1 0 0 0 0];
    answerValue = 1;
elseif centroid > 35 && centroid <= 73.5
    answer = [0 1 0 0 0];
    answerValue = 2;
elseif centroid > 73.5 && centroid <= 112
    answer = [0 0 1 0 0];
    answerValue = 3;
elseif centroid > 112 && centroid <= 150.5
    answer = [0 0 0 1 0];
    answerValue = 4;
else
    answer = [0 0 0 0 1];
    answerValue = 5;

end

