clc, clear variables, close all;

imgFilename = 'Imagens/p21.jpg';
% imgFilename = 'Imagens/p22.jpg';
% imgFilename = 'Imagens/p23.jpg';

% imgFilename = 'Imagens/p31.jpg';
% imgFilename = 'Imagens/p32.jpg';
% imgFilename = 'Imagens/p33.jpg';
% imgFilename = 'Imagens/p34.jpg';

img = imread(imgFilename);


imgGray = rgb2gray(img);
imgGray = imgaussfilt(imgGray, 2.5);

imgBW = imbinarize(imgGray, 'adaptive', 'ForegroundPolarity', 'bright', 'sensitivity', 0.8);
imgBW = imcomplement(imgBW);

[ refPoints, imgMorph] = findRefPoints(imgBW);
% Transformation
imgTransformed = findImageTransformed(imgBW, refPoints);

figure
subplot(2,2,1), imshow(img)
subplot(2,2,2), imshow(imgBW)
subplot(2,2,3), imshow(imgMorph)
subplot(2,2,4), imshow(imgTransformed)
